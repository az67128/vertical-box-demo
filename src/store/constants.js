export const IS_TEST = /(localhost|192\.168|az67128.gitlab.io)/.test(window.location.href)
export const IS_VK = /vk_app_id/.test(window.location.href)
export const IS_MOBILE_VK = /vk_platform\=mobile/.test(window.location.href)

export const BASE_URL = /(localhost|192\.168)/.test(window.location.href) ? '.' : '.'
// : "https://az67128.gitlab.io/svelte-vertical";

export const FILMS = [
  {
    id: 1,
    filmUrl:
      'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/36499fa2-a760-4e75-b120-e2b7ad11a9c5/BOX_1_original.ism/manifest(format=m3u8-aapl)',
    episodes: [{ id: 1, start: 0, end: 7908, markup: '/data/box9.vxml' }],
  },
  {
    id: 8,
    cover: 'elkilast.png',
    title: 'Ёлки Последние',
    description: '2018, Россия, Комедии, Русские',
    kinopisk: '5.6',
    filmUrl:
      'https://ongocinemamedia-euno.streaming.media.azure.net//f41a7fab-b88f-49b2-a961-6518a3a73d28/elki-7-1080-2.ism/manifest(format=m3u8-aapl,encryption=cbc)',
    height: 800,
    episodes: [
      {
        id: 1,
        title: 'Серия 1',
        markup: '/data/elki7/YOLKI_7_1_VERTICAL-24fps-1080x2340.vxml',
        start: 0,
        end: 15381,
        cover: '/elki7/7_series_1.jpg',
      },
      {
        id: 2,
        title: 'Серия 2',
        markup: '/data/elki7/YOLKI_7_2_VERTICAL-24fps-1080x2340.vxml',
        start: 15383,
        end: 31116,
        cover: '/elki7/7_series_2.jpg',
      },
      {
        id: 3,
        title: 'Серия 3',
        markup: '/data/elki7/YOLKI_7_3_VERTICAL-24fps-1080x2340.vxml',
        start: 31118,
        end: 51441,
        cover: '/elki7/7_series_3.jpg',
      },
      {
        id: 4,
        title: 'Серия 4',
        markup: '/data/elki7/YOLKI_7_4_VERTICAL-24fps-1080x2340.vxml',
        start: 51445,
        end: 68170,
        cover: '/elki7/7_series_4.jpg',
      },
      {
        id: 5,
        title: 'Серия 5',
        markup: '/data/elki7/YOLKI_7_5_VERTICAL-24fps-1080x2340.vxml',
        start: 68172,
        end: 82373,
        cover: '/elki7/7_series_5.jpg',
      },
      {
        id: 6,
        title: 'Серия 6',
        markup: '/data/elki7/YOLKI_7_6_VERTICAL-24fps-1080x2340.vxml',
        start: 82375,
        end: 96967,
        cover: '/elki7/7_series_6.jpg',
      },
      {
        id: 7,
        title: 'Серия 7',
        markup: '/data/elki7/YOLKI_7_7_VERTICAL-24fps-1080x2340.vxml',
        start: 96969,
        end: 117100,
        cover: '/elki7/7_series_7.jpg',
      },
      {
        id: 8,
        title: 'Серия 8',
        markup: '/data/elki7/YOLKI_7_8_VERTICAL-24fps-1080x2340.vxml',
        start: 117107,
        end: 132027,
        cover: '/elki7/7_series_8.jpg',
      },
    ],
    markupUrl: '/data/elkilast.vxml',
    year: 2018,
  },
]

import { IS_TEST } from "../store/constants";

const sendMetrik = (name, payload) => {
  const consoleMetrik = (id, type, name, payload) => console.log(name, payload);
  const ym = IS_TEST ? consoleMetrik : window.ym || consoleMetrik;
  // const ym = window.ym || consoleMetrik;

  ym(53183122, "reachGoal", `Elki.${name}`, payload);
};
export default sendMetrik;

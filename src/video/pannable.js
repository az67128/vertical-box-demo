import { spring } from "svelte/motion";

export default function pannable(node) {
  
  let startX;
  let startY;
  let x;
  let y;

  function handleMousedown(e) {
    if (e.target.tagName !== 'VIDEO' ) return
    const event = e.touches ? e.touches[0] : e;
    x = startX = event.clientX;
    y = startY = event.clientY;
    
    node.dispatchEvent(
      new CustomEvent('panstart', {
        detail: { x, y },
      })
    );

    window.addEventListener('mousemove', handleMousemove);
    window.addEventListener('touchmove', handleMousemove);
    window.addEventListener('mouseup', handleMouseup);
    window.addEventListener('touchend', handleMouseup);
  }

  function handleMousemove(e) {
    const event = e.touches ? e.touches[0] : e;
    const dx = event.clientX - x;
    const dy = event.clientY - y;
    x = event.clientX;
    y = event.clientY;

    if (Math.abs(event.clientX - startX) < window.innerWidth * 0.15) return;

    node.dispatchEvent(
      new CustomEvent('panmove', {
        detail: { x, y, dx, dy },
      })
    );
  }

  function handleMouseup(e) {
    const event = e.changedTouches ? e.changedTouches[0] : e;

    x = event.clientX;
    y = event.clientY;

    node.dispatchEvent(
      new CustomEvent('panend', {
        detail: { x, y },
      })
    );

    window.removeEventListener('mousemove', handleMousemove);
    window.removeEventListener('touchmove', handleMousemove);
    window.removeEventListener('mouseup', handleMouseup);
    window.removeEventListener('touchend', handleMouseup);
  }

  node.addEventListener('mousedown', handleMousedown);
  node.addEventListener('touchstart', handleMousedown);

  return {
    destroy() {
      node.removeEventListener('mousedown', handleMousedown);
      node.removeEventListener('touchstart', handleMousedown);
    },
  };
}


let isPanStarted = false;
export const coords = spring(
  { x: 0, y: 0 },
  {
    stiffness: 0.2,
    damping: 0.4,
  }
);

export function handlePanStart() {
  coords.stiffness = coords.damping = 1;
  isPanStarted = true;
}

export function handlePanMove(event, seeking) {
  if (seeking) return;
  if (window.innerWidth > window.innerHeight) return;
  if (isPanStarted) {
   
    isPanStarted = false;
  }
  coords.update(($coords) => ({
    x: $coords.x + event.detail.dx * 1.5,
    y: $coords.y + event.detail.dy * 1.5,
  }));
}

export function handlePanEnd(event) {
  coords.stiffness = 0.2;
  coords.damping = 0.4;
  coords.set({ x: 0, y: 0 });
}
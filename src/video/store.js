import {writable} from 'svelte/store';

export const markup = writable({});
export const frameRate = writable(24);
export const frame = writable(0);